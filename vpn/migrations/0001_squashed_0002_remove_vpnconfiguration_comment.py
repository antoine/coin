# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import netfields.fields
import coin.validation
import coin.mixins


class Migration(migrations.Migration):

    replaces = [('vpn', '0001_initial'), ('vpn', '0002_remove_vpnconfiguration_comment')]

    dependencies = [
        ('configuration', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='VPNConfiguration',
            fields=[
                ('configuration_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='configuration.Configuration')),
                ('activated', models.BooleanField(default=False, verbose_name='activ\xe9')),
                ('login', models.CharField(help_text='leave empty for automatic generation', unique=True, max_length=50, verbose_name='identifiant', blank=True)),
                ('password', models.CharField(max_length=256, null=True, verbose_name='mot de passe', blank=True)),
                ('ipv4_endpoint', netfields.fields.InetAddressField(validators=[coin.validation.validate_v4], max_length=39, blank=True, help_text='Adresse IPv4 utilis\xe9e par d\xe9faut sur le VPN', null=True, verbose_name='IPv4')),
                ('ipv6_endpoint', netfields.fields.InetAddressField(validators=[coin.validation.validate_v6], max_length=39, blank=True, help_text='Adresse IPv6 utilis\xe9e par d\xe9faut sur le VPN', null=True, verbose_name='IPv6')),
            ],
            options={
                'verbose_name': 'VPN',
            },
            bases=(coin.mixins.CoinLdapSyncMixin, 'configuration.configuration'),
        ),
        migrations.CreateModel(
            name='LdapVPNConfig',
            fields=[
            ],
            options={
                'managed': False,
            },
        ),
    ]
