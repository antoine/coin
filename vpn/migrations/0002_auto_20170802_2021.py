# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vpn', '0001_squashed_0002_remove_vpnconfiguration_comment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='vpnconfiguration',
            options={'verbose_name': 'VPN', 'verbose_name_plural': 'VPN'},
        ),
        migrations.AlterField(
            model_name='vpnconfiguration',
            name='login',
            field=models.CharField(help_text='Laisser vide pour une g\xe9n\xe9ration automatique', unique=True, max_length=50, verbose_name='identifiant', blank=True),
        ),
    ]
