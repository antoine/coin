# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='MaillingList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_name', models.CharField(help_text='c\'est l\'identifiant qui servira \xe0 communiquer avec le syst\xe8me de mailling-list(typiquement, la partie avant le "@" dans l\'adress )', max_length=50, verbose_name='identifiant technique')),
                ('email', models.EmailField(max_length=254, verbose_name="adresse mail d'envoi")),
                ('verbose_name', models.CharField(help_text="Nom affich\xe9 dans l'interface membre", max_length=130, verbose_name='nom complet')),
                ('description', models.TextField()),
            ],
            options={
                'verbose_name': 'liste mail',
                'verbose_name_plural': 'listes mail',
            },
        ),
        migrations.CreateModel(
            name='MaillingListSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('maillinglist', models.ForeignKey(to='maillists.MaillingList')),
                ('member', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'abonnement \xe0 une liste mail',
                'verbose_name_plural': 'abonnements \xe0 des listes mail',
            },
        ),
        migrations.AddField(
            model_name='maillinglist',
            name='subscribers',
            field=models.ManyToManyField(related_name='subscribed_maillinglists', verbose_name='abonn\xe9\xb7e\xb7s', to=settings.AUTH_USER_MODEL, through='maillists.MaillingListSubscription', blank=True),
        ),
    ]
