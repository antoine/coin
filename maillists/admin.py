# -*- coding: utf-8 -*-

from __future__ import unicode_literals


import autocomplete_light
from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect

from .models import MaillingList, MaillingListSubscription, SyncCommandError
import coin.members.admin


class AddMaillingListSubscriptionInline(admin.StackedInline):
    model = MaillingListSubscription
    extra = 0
    fields = ('member', 'maillinglist')
    verbose_name_plural = "Ajouter un abonnement à une liste mail"
    verbose_name = "abonnement"

    form = autocomplete_light.modelform_factory(MaillingListSubscription, fields='__all__')

    def get_queryset(self, request):
        qs = super(AddMaillingListSubscriptionInline, self).get_queryset(request)
        return qs.none()

    def has_delete_permission(self, request, obj=None):
        return False


class AddSubscriberInline(AddMaillingListSubscriptionInline):
    verbose_name_plural = "Ajouter des abonné·e·s"
    verbose_name = "abonné·e"


class MaillingListSubscriptionInline(admin.TabularInline):
    model = MaillingListSubscription

    readonly_fields = ('member', 'maillinglist',)

    def has_add_permission(self, request, obj=None):
        return False


class SubscribersInline(MaillingListSubscriptionInline):
    fields = ('member', 'email', 'maillinglist',)
    readonly_fields = ('member', 'email', 'maillinglist',)
    verbose_name_plural = "Abonné·e·s"
    verbose_name = "abonné·e"

    def email(self, instance):
        return instance.member.email


class MaillingListAdmin(admin.ModelAdmin):
    list_display = ('email', 'verbose_name')
    actions = ['sync_to_server']

    def sync_to_server(self, request, queryset):
        for _list in queryset.all():
            try:
                _list.sync_to_list_server()
            except Exception as e:
                messages.error(
                    request,
                    'Impossible de synchroniser la liste {} : "{}"'.format(
                        _list, e))
            else:
                messages.success(
                    request,
                    'Liste {} synchronisée vers le serveur'.format(
                        _list.email))
    sync_to_server.short_description = (
        'Synchroniser les listes sélectionnées vers le serveur')

    inlines = [AddSubscriberInline, SubscribersInline]

    def change_view(self, request, object_id, *args, **kwargs):
        try:
            return super(MaillingListAdmin, self).change_view(
                request, object_id, *args, **kwargs)
        except SyncCommandError as e:
            try:
                ml = MaillingList.objects.get(pk=object_id)
                ml_name = "La liste mail « {} »".format(ml.short_name)
            except MaillingList.DoesNotExist:
                ml_name = "La nouvelle liste mail"
            messages.error(
                request,
                "{} n'a pas pu être synchronisée".format(ml_name) +
                " vers le serveur de listes : « {} ».".format(e))
            return HttpResponseRedirect(request.path)


admin.site.register(MaillingList, MaillingListAdmin)

# Enrich the MemberAdmin with maillists-related information
_MemberAdmin = admin.site._registry[coin.members.admin.Member].__class__


class MemberAdmin(_MemberAdmin):
    inlines = _MemberAdmin.inlines + [
        MaillingListSubscriptionInline,
        AddMaillingListSubscriptionInline,
    ]

    def change_view(self, request, *args, **kwargs):
        try:
            return super(MemberAdmin, self).change_view(
                request, *args, **kwargs)
        except SyncCommandError as e:
            messages.error(
                request,
                "Les listes mails n'ont pas pu être synchronisées" +
                " vers le serveur de listes : « {} ».".format(e))
            return HttpResponseRedirect(request.path)


admin.site.unregister(coin.members.admin.Member)
admin.site.register(coin.members.admin.Member, MemberAdmin)
