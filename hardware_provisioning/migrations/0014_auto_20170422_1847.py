# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0013_auto_20161110_2246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='buy_date',
            field=models.DateField(null=True, verbose_name='date d\u2019achat', blank=True),
        ),
    ]
