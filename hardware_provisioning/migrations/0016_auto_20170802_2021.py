# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import hardware_provisioning.fields


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0015_auto_20170802_1701'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='deployed',
        ),
        migrations.AlterField(
            model_name='item',
            name='mac_address',
            field=hardware_provisioning.fields.MACAddressField(null=True, max_length=17, blank=True, help_text='pr\xe9f\xe9rable au n\xb0 de s\xe9rie si possible', unique=True, verbose_name='adresse MAC'),
        ),
        migrations.AlterField(
            model_name='item',
            name='owner',
            field=models.ForeignKey(related_name='items', blank=True, to=settings.AUTH_USER_MODEL, help_text="dans le cas de mat\xe9riel n'appartenant pas \xe0 l'association", null=True, verbose_name='Propri\xe9taire'),
        ),
        migrations.AlterField(
            model_name='item',
            name='serial',
            field=models.CharField(null=True, max_length=250, blank=True, help_text='ou toute autre r\xe9f\xe9rence unique', unique=True, verbose_name='N\xb0 de s\xe9rie'),
        ),
    ]
