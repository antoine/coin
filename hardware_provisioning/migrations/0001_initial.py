# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import hardware_provisioning.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('designation', models.CharField(max_length=100, verbose_name='d\xe9signation')),
                ('mac_address', hardware_provisioning.fields.MACAddressField(max_length=17, null=True, verbose_name='addresse MAC', blank=True)),
                ('buy_date', models.DateTimeField(verbose_name='date d\u2019achat')),
                ('comment', models.TextField(null=True, verbose_name='commentaire', blank=True)),
            ],
            options={
                'verbose_name': 'objet',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='nom')),
            ],
            options={
                'verbose_name': 'type d\u2019objet',
                'verbose_name_plural': 'types d\u2019objet',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Loan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('loan_date', models.DateTimeField(verbose_name='date de pr\xeat')),
                ('loan_date_end', models.DateTimeField(null=True, verbose_name='date de fin de pr\xeat', blank=True)),
                ('location', models.CharField(max_length=100, null=True, verbose_name='emplacement', blank=True)),
                ('item', models.ForeignKey(related_name='loans', verbose_name='objet', to='hardware_provisioning.Item')),
                ('user', models.ForeignKey(related_name='loans', verbose_name='membre', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'pr\xeat d\u2019objet',
                'verbose_name_plural': 'pr\xeats d\u2019objets',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='item',
            name='type',
            field=models.ForeignKey(related_name='items', verbose_name='type de mat\xe9riel', to='hardware_provisioning.ItemType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='user_in_charge',
            field=models.ForeignKey(related_name='items', verbose_name='membre responsable', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
