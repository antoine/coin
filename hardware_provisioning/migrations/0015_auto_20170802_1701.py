# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import hardware_provisioning.fields


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0014_auto_20170422_1847'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='deployed',
            field=models.BooleanField(default=False, help_text='Cocher si le mat\xe9riel est en production', verbose_name='d\xe9ploy\xe9'),
        ),
        migrations.AlterField(
            model_name='item',
            name='mac_address',
            field=hardware_provisioning.fields.MACAddressField(null=True, max_length=17, blank=True, help_text='Pr\xe9f\xe9rable au n\xb0 de s\xe9rie si possible', unique=True, verbose_name='addresse MAC'),
        ),
        migrations.AlterField(
            model_name='item',
            name='owner',
            field=models.ForeignKey(related_name='items', blank=True, to=settings.AUTH_USER_MODEL, help_text="Dans le cas de mat\xe9riel n'appartenant pas \xe0 l'association", null=True, verbose_name='Propri\xe9taire'),
        ),
    ]
