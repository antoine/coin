# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.loan_list, name='loan-list'),
    url(r'^items/list$', views.item_list, name='item-list'),
    url(r'^items/(?P<pk>[0-9]+)/borrow$', views.item_borrow, name='item-borrow'),
    url(r'^(?P<pk>[0-9]+)/return$', views.loan_return, name='loan-return'),
    url(r'^(?P<pk>[0-9]+)/transfer$', views.loan_transfer, name='loan-transfer'),
    url(r'^(?P<pk>[0-9]+)$', views.loan_detail, name='loan-detail'),
]
