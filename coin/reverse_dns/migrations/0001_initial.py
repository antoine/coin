# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators
import netfields.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('resources', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NameServer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dns_name', models.CharField(help_text='Exemple : ns1.example.com', max_length=255, verbose_name='nom du serveur')),
                ('description', models.CharField(help_text='Exemple : Mon serveur de noms principal', max_length=255, verbose_name='description du serveur', blank=True)),
                ('owner', models.ForeignKey(verbose_name='propri\xe9taire', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'serveur de noms',
                'verbose_name_plural': 'serveurs de noms',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReverseDNSEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', netfields.fields.InetAddressField(unique=True, max_length=39, verbose_name='adresse IP')),
                ('reverse', models.CharField(help_text="Nom \xe0 associer \xe0 l'adresse IP", max_length=255, verbose_name='reverse')),
                ('ttl', models.IntegerField(default=3600, help_text='en secondes', verbose_name='TTL', validators=[django.core.validators.MinValueValidator(60)])),
                ('ip_subnet', models.ForeignKey(verbose_name='sous-r\xe9seau IP', to='resources.IPSubnet')),
            ],
            options={
                'verbose_name': 'entr\xe9e DNS inverse',
                'verbose_name_plural': 'entr\xe9es DNS inverses',
            },
            bases=(models.Model,),
        ),
    ]
