# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db.models import Q
from polymorphic.admin import PolymorphicChildModelAdmin

from coin.members.models import Member
from coin.offers.models import Offer, OfferSubscription
from coin.offers.offersubscription_filter import\
            OfferSubscriptionTerminationFilter,\
            OfferSubscriptionCommitmentFilter
from coin.offers.forms import OfferAdminForm
import autocomplete_light


class OfferAdmin(admin.ModelAdmin):
    list_display = ('get_configuration_type_display', 'name', 'reference', 'billing_period', 'period_fees',
                    'initial_fees')
    list_display_links = ('name',)
    list_filter = ('configuration_type',)
    search_fields = ['name']
    form = OfferAdminForm

    # def get_readonly_fields(self, request, obj=None):
    #     if obj:
    #         return ['backend',]
    #     else:
    #         return []


class OfferSubscriptionAdmin(admin.ModelAdmin):
    list_display = ('get_subscription_reference', 'member', 'offer',
                    'subscription_date', 'commitment', 'resign_date')
    list_display_links = ('member','offer')
    list_filter = ( OfferSubscriptionTerminationFilter,
                    OfferSubscriptionCommitmentFilter,
                    'offer', 'member')
    search_fields = ['member__first_name', 'member__last_name', 'member__email',
                     'member__nickname']
    
    fields = (
                'member',
                'offer',
                'subscription_date',
                'commitment',
                'resign_date',
                'comments'
             )
    # Si c'est un super user on renvoie un formulaire avec tous les membres et toutes les offres (donc autocomplétion pour les membres)
    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            kwargs['form'] = autocomplete_light.modelform_factory(OfferSubscription, fields='__all__')
        return super(OfferSubscriptionAdmin, self).get_form(request, obj, **kwargs)

    # Si pas super user on restreint les membres et offres accessibles
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if not request.user.is_superuser:
            if db_field.name == "member":
                kwargs["queryset"] = Member.objects.manageable_by(request.user)
            if db_field.name == "offer":
                kwargs["queryset"] = Offer.objects.filter(id__in=[p.id for p in Offer.objects.manageable_by(request.user)])
        return super(OfferSubscriptionAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    # Si pas super user on restreint la liste des offres que l'on peut voir
    def get_queryset(self, request):
        qs = super(OfferSubscriptionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else:
            offers = Offer.objects.manageable_by(request.user)
            return qs.filter(offer__in=offers)

    def get_inline_instances(self, request, obj=None):
        """
        Si en edition, alors affiche en inline le formulaire de la configuration
        correspondant à l'offre choisie
        """
        if obj is not None:
            for item in PolymorphicChildModelAdmin.__subclasses__():
                if (item.base_model.__name__ == obj.offer.configuration_type):
                    return [item.inline(self.model, self.admin_site)]
        return []

admin.site.register(Offer, OfferAdmin)
admin.site.register(OfferSubscription, OfferSubscriptionAdmin)
