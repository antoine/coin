# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('billing', '0008_auto_20170802_2021'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentAllocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.DecimalField(null=True, verbose_name='montant', max_digits=5, decimal_places=2)),
            ],
        ),
        migrations.AddField(
            model_name='invoice',
            name='date_last_reminder_email',
            field=models.DateTimeField(null=True, verbose_name='Date du dernier email de relance envoy\xe9', blank=True),
        ),
        migrations.AddField(
            model_name='payment',
            name='label',
            field=models.CharField(default='', max_length=500, null=True, verbose_name='libell\xe9', blank=True),
        ),
        migrations.AddField(
            model_name='payment',
            name='member',
            field=models.ForeignKey(related_name='payments', on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name='membre'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='invoice',
            field=models.ForeignKey(related_name='payments', verbose_name='facture associ\xe9e', blank=True, to='billing.Invoice', null=True),
        ),
        migrations.AddField(
            model_name='paymentallocation',
            name='invoice',
            field=models.ForeignKey(related_name='allocations', verbose_name='facture associ\xe9e', to='billing.Invoice'),
        ),
        migrations.AddField(
            model_name='paymentallocation',
            name='payment',
            field=models.ForeignKey(related_name='allocations', verbose_name='facture associ\xe9e', to='billing.Payment'),
        ),
    ]
