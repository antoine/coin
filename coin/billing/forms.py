# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.core.exceptions import ValidationError
from django import forms

def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.csv']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')

class WizardImportPaymentCSV(forms.Form):

    csv_file = forms.FileField(validators=[validate_file_extension])
