# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib.auth.forms import PasswordResetForm, ReadOnlyPasswordHashField
from django.forms.utils import ErrorList
from django.forms.forms import BoundField

from coin.members.models import Member

from registration.forms import RegistrationForm


class MemberRegistrationForm(RegistrationForm):
    # Protect against robot
    trap = forms.CharField(required=False, label='Trap',
            widget=forms.TextInput(attrs={'style' : 'display:none'}),
        help_text="Si vous êtes humain ignorez ce champ")

    def __init__(self, *args, **kwargs):
        super(MemberRegistrationForm, self).__init__(*args, **kwargs)

        for fieldname in ['email', 'organization_name', 'password2']:
            self.fields[fieldname].help_text = None

    def is_valid(self):
         valid = super(MemberRegistrationForm,self).is_valid()
         avoid_trap = not self.data['trap']
         if valid and avoid_trap:
             return True
         else:
             return False

    def as_p(self):
        """"
        We rewrite the as_p method to apply a style on the <p> tag related to
        trap field. Indeed, it seeems there is no cleaner way to do it.
        """
        def css_classes(self, extra_classes=None):
            return 'captcha'

        func_type = type(BoundField.css_classes)
        self['trap'].css_classes = func_type(css_classes, self, BoundField)
        return super(MemberRegistrationForm, self).as_p()

    class Meta:
        model = Member
        fields = ['type', 'first_name', 'last_name',
                  'organization_name', 'email', 'username', 'trap']

class MemberCreationForm(forms.ModelForm):

    """
    This form was inspired from django.contrib.auth.forms.UserCreationForm
    and adapted to coin specificities
    """
    username = forms.RegexField(required=False,
                                label="Nom d'utilisateur", max_length=30, regex=r"^[\w.@+-]+$",
                                help_text="Laisser vide pour le générer automatiquement à partir du "
                                "nom d'usage, nom et prénom, ou nom de l'organisme")
    password = forms.CharField(
        required=False, label='Mot de passe', widget=forms.PasswordInput,
        help_text="Laisser vide et envoyer un mail de bienvenue pour que "
        "l'utilisateur choisisse son mot de passe lui-même")

    class Meta:
        model = Member
        fields = '__all__'

    def save(self, commit=True):
        """
        Save member, then set his password
        """
        member = super(MemberCreationForm, self).save(commit=False)
        member.set_password(self.cleaned_data["password"])
        if commit:
            member.member()
        return member


class AbstractMemberChangeForm(forms.ModelForm):
    """
    This form was inspired from django.contrib.auth.forms.UserChangeForm
    and adapted to coin specificities
    """

    class Meta:
        model = Member
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AbstractMemberChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

    def clean_username(self):
        # idem clean_password
        return self.initial["username"]


class AdminMemberChangeForm(AbstractMemberChangeForm):
    password = ReadOnlyPasswordHashField()


class SpanError(ErrorList):
    def __unicode__(self):
        return self.as_spans()
    def __str__(self):
        return self.as_spans()
    def as_spans(self):
        if not self: return ''
        return ''.join(['<span class="error">%s</span>' % e for e in self])

class PersonMemberChangeForm(AbstractMemberChangeForm):
    """
    Form use to allow natural person to change their info
    """
    class Meta:
        model = Member
        fields = ['first_name', 'last_name', 'email', 'nickname',
                  'home_phone_number', 'mobile_phone_number',
                  'address', 'postal_code', 'city', 'country']

    def __init__(self, *args, **kwargs):
        super(PersonMemberChangeForm, self).__init__(*args, **kwargs)
        self.error_class = SpanError
        for fieldname in self.fields:
            self.fields[fieldname].help_text = None


class OrganizationMemberChangeForm(AbstractMemberChangeForm):
    """
    Form use to allow organization to change their info
    """
    class Meta:
        model = Member
        fields = ['organization_name', 'email', 'nickname',
                  'home_phone_number', 'mobile_phone_number',
                  'address', 'postal_code', 'city', 'country']

    def __init__(self, *args, **kwargs):
        super(OrganizationMemberChangeForm, self).__init__(*args, **kwargs)
        self.error_class = SpanError
        for fieldname in self.fields:
            self.fields[fieldname].help_text = None

class MemberPasswordResetForm(PasswordResetForm):
    pass

