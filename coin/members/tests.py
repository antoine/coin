# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

import ldapdb
from datetime import date
from cStringIO import StringIO
from dateutil.relativedelta import relativedelta
from freezegun import freeze_time
import unittest
from freezegun import freeze_time

from django import db
from django.conf import settings
from django.test import TestCase, Client
from django.core import mail, management
from django.core.exceptions import ValidationError

from coin.members.models import Member, MembershipFee, LdapUser
from coin.offers.models import OfferSubscription, Offer
from coin.validation import chatroom_url_validator


@unittest.skipIf(not settings.LDAP_ACTIVATE, "LDAP disabled")
class LDAPMemberTests(TestCase):
    def test_when_creating_member_a_ldapuser_is_also_created_with_same_data(self):
        """
        Test que lors de la création d'un nouveau membre, une entrée
        correspondante est bien créée dans le LDAP et qu'elle contient
        les mêmes données.
        Cela concerne le nom et le prénom
        """

        #~ Créé un membre
        first_name = 'Gérard'
        last_name = 'Majax'
        username = MemberTestsUtils.get_random_username()
        member = Member(first_name=first_name,
                        last_name=last_name,
                        username=username)
        member.save()

        #~ Récupère l'utilisateur LDAP et fait les tests
        ldap_user = LdapUser.objects.get(pk=username)

        self.assertEqual(ldap_user.first_name, first_name)
        self.assertEqual(ldap_user.last_name, last_name)
        self.assertEqual(ldap_user.pk, username)

        member.delete()

    def test_when_modifiying_member_corresponding_ldap_user_is_also_modified_with_same_data(self):
        """
        Test que lorsque l'on modifie un membre, l'utilisateur LDAP
        correspondant est bien modifié
        Cela concerne le no met le prénom
        """
        #~ Créé un membre
        first_name = 'Ronald'
        last_name = 'Mac Donald'
        username = MemberTestsUtils.get_random_username()
        member = Member(first_name=first_name,
                        last_name=last_name, username=username)
        member.save()

        #~  Le modifie
        new_first_name = 'José'
        new_last_name = 'Bové'
        member.first_name = new_first_name
        member.last_name = new_last_name
        member.save()

        #~ Récupère l'utilisateur LDAP et fait les tests
        ldap_user = LdapUser.objects.get(pk=username)

        self.assertEqual(ldap_user.first_name, new_first_name)
        self.assertEqual(ldap_user.last_name, new_last_name)

        member.delete()

    # def test_when_creating_member_corresponding_ldap_user_is_in_coin_ldap_group(self):
    #     """
    #     Test que l'utilisateur Ldap fraichement créé est bien dans le group "coin"
    #     Et que lors de la supression d'un membre, l'utilisateur LDAP correspondant
    #     est bien retiré du groupe.
    #     """
    # ~ Créé un membre
    #     username = MemberTestsUtils.get_random_username()
    #     member = Member(first_name='Canard',
    #                     last_name='WC', username=username)
    #     member.save()

    # ~ Récupère le group "coin" et test que l'utilisateur y est présent
    #     ldap_group = LdapGroup.objects.get(pk="coin")
    #     self.assertEqual(username in ldap_group.members, True)

    # ~ Supprime l'utilisateur
    #     member.delete()

    # ~ Récupère le group "coin" et test que l'utilisateur n'y est plus
    #     ldap_group = LdapGroup.objects.get(pk="coin")
    #     self.assertEqual(username in ldap_group.members, False)

    #     LdapUser.objects.get(pk=username).delete()

    def test_change_password_and_auth(self):
        """
        Test que la fonction change_password de member fonctionne et permet
        l'authentification avec le nouveau mot de passe
        """
        username = MemberTestsUtils.get_random_username()
        password = "1234"

        #~ Créé un nouveau membre
        member = Member(first_name='Passe-partout',
                        last_name='Du fort Boyard', username=username)
        member.save()

        #~ Récupère l'utilisateur LDAP
        ldap_user = LdapUser.objects.get(pk=username)

        #~ Change son mot de passe
        member.set_password(password)
        member.save()

        #~ Test l'authentification django
        c = Client()
        self.assertEqual(c.login(username=username, password=password), True)

        # Test l'authentification ldap
        import ldap
        ldap_conn_settings = db.connections['ldap'].settings_dict
        l = ldap.initialize(ldap_conn_settings['NAME'])
        options = ldap_conn_settings.get('CONNECTION_OPTIONS', {})
        for opt, value in options.items():
            l.set_option(opt, value)

        if ldap_conn_settings.get('TLS', False):
            l.start_tls_s()

        # Raise "Invalid credentials" exception if auth fail
        l.simple_bind_s(ldap_conn_settings['USER'],
                        ldap_conn_settings['PASSWORD'])

        l.unbind_s()

        member.delete()

    def test_when_creating_member_ldap_display_name_is_well_defined(self):
        """
        Lors de la création d'un membre, le champ "display_name" du LDAP est
        prenom + nom
        """
        first_name = 'Gérard'
        last_name = 'Majax'
        username = MemberTestsUtils.get_random_username()
        member = Member(first_name=first_name,
                        last_name=last_name, username=username)
        member.save()

        #~ Récupère l'utilisateur LDAP
        ldap_user = LdapUser.objects.get(pk=username)

        self.assertEqual(ldap_user.display_name, '%s %s' %
                         (first_name, last_name))

        member.delete()


    def test_when_saving_member_and_ldap_fail_dont_save(self):
        """
        Test que lors de la sauvegarde d'un membre et que la sauvegarde en LDAP
        échoue (ici mauvais mot de passe), rien n'est sauvegardé en base
        """

        # Fait échouer le LDAP en définissant un mauvais mot de passe
        for dbconnection in db.connections.all():
            if (type(dbconnection) is
                    ldapdb.backends.ldap.base.DatabaseWrapper):
                dbconnection.settings_dict[
                    'PREVIOUSPASSWORD'] = dbconnection.settings_dict['PASSWORD']
                dbconnection.settings_dict['PASSWORD'] = 'wrong password test'

        # Créé un membre
        first_name = 'Du'
        last_name = 'Pont'
        username = MemberTestsUtils.get_random_username()
        member = Member(first_name=first_name,
                        last_name=last_name, username=username)

        # Le sauvegarde en base de donnée
        # Le save devrait renvoyer une exception parceque le LDAP échoue
        self.assertRaises(Exception, member.save)

        # On s'assure, malgré l'exception, que le membre n'est pas en base
        with self.assertRaises(Member.DoesNotExist):
            Member.objects.get(username=username)

        # Restaure le mot de passe pour les tests suivants
        for dbconnection in db.connections.all():
            if (type(dbconnection) is
                    ldapdb.backends.ldap.base.DatabaseWrapper):
                dbconnection.settings_dict[
                    'PASSWORD'] = dbconnection.settings_dict['PREVIOUSPASSWORD']

    # def test_when_user_login_member_user_field_is_updated(self):
    #     """
    #     Test que lorqu'un utilisateur se connect, le champ user du membre
    #     correspondant est mis à jour convenablement
    #     """
    # Créé un membre
    #     first_name = 'Du'
    #     last_name = 'Pond'
    #     password = '1234'
    #     username = MemberTestsUtils.get_random_username()
    #     member = Member(first_name=first_name,
    #                     last_name=last_name, username=username)
    #     member.save()
    #     member.change_password(password)

    # Vérifie que user non définit
    #     self.assertIsNone(member.user)

    # Connection
    #     c = Client()
    #     self.assertEqual(c.login(username=username, password=password), True)

    # Vérifie que user définit
    #     member = Member.objects.get(username=username)
    #     self.assertIsNotNone(member.user)

    #     LdapUser.objects.get(pk=member.username).delete()


class MemberTests(TestCase):
    def test_when_creating_member_username_is_well_defined(self):
        """
        Lors de la création d'un membre, le champ "username", s'il n'est pas
        définit doit être généré avec les contraintes suivantes :
        premières lettres du prénom + nom le tout en minuscule,
        sans caractères accentués et sans espaces.
        """
        random = os.urandom(4).encode('hex')
        first_name = 'Gérard-Étienne'
        last_name = 'Majax de la Boétie!B' + random

        control = 'gemajaxdelaboetieb' + random
        control = control[:30]

        member = Member(first_name=first_name, last_name=last_name)
        member.save()

        self.assertEqual(member.username, control)

        member.delete()

    def test_when_creating_member_with_username_already_exists_username_is_incr(self):
        """
        Lors de la création d'un membre, test si le username existe déjà,
        renvoi avec un incrément à la fin
        """
        random = os.urandom(4).encode('hex')

        member1 = Member(first_name='Hervé', last_name='DUPOND' + random, email='hdupond@coin.org')
        member1.save()
        self.assertEqual(member1.username, 'hdupond' + random)

        member2 = Member(first_name='Henri', last_name='DUPOND' + random, email='hdupond2@coin.org')
        member2.save()
        self.assertEqual(member2.username, 'hdupond' + random + '2')

        member3 = Member(first_name='Hector', last_name='DUPOND' + random, email='hdupond3@coin.org')
        member3.save()
        self.assertEqual(member3.username, 'hdupond' + random + '3')

        member1.delete()
        member2.delete()
        member3.delete()

    def test_when_creating_legal_entity_organization_name_is_used_for_username(self):
        """
        Lors de la créatio d'une entreprise, son nom doit être utilisée lors de
        la détermination automatique du username
        """
        random = os.urandom(4).encode('hex')
        member = Member(type='legal_entity', organization_name='ILLYSE' + random, email='illyse@coin.org')
        member.save()
        self.assertEqual(member.username, 'illyse' + random)
        member.delete()

    def test_when_creating_member_with_nickname_it_is_used_for_username(self):
        """
        Lors de la création d'une personne, qui a un pseudo, celui-ci est utilisé en priorité
        """
        random = os.urandom(4).encode('hex')
        member = Member(first_name='Richard', last_name='Stallman', nickname='rms' + random, email='illyse@coin.org')
        member.save()
        self.assertEqual(member.username, 'rms' + random)

        member.delete()

    def test_member_end_date_of_memberhip(self):
        """
        Test que end_date_of_membership d'un membre envoi bien la date de fin d'adhésion
        """
        # Créer un membre
        first_name = 'Tin'
        last_name = 'Tin'
        username = MemberTestsUtils.get_random_username()
        member = Member(first_name=first_name,
                        last_name=last_name, username=username)
        member.save()

        start_date = date.today()
        end_date = start_date + relativedelta(years=+1)

        # Créé une cotisation
        membershipfee = MembershipFee(member=member, amount=20,
                                      start_date=start_date,
                                      end_date=end_date)
        membershipfee.save()

        self.assertEqual(member.end_date_of_membership(), end_date)

    def test_member_is_paid_up(self):
        """
        Test l'état "a jour de cotisation" d'un adhérent.
        """
        # Créé un membre
        first_name = 'Capitain'
        last_name = 'Haddock'
        username = MemberTestsUtils.get_random_username()
        member = Member(first_name=first_name,
                        last_name=last_name, username=username)
        member.save()

        start_date = date.today()
        end_date = start_date + relativedelta(years=+1)

        # Test qu'un membre sans cotisation n'est pas à jour
        self.assertEqual(member.is_paid_up(), False)

        # Créé une cotisation passée
        membershipfee = MembershipFee(member=member, amount=20,
                                      start_date=date.today() +
                                      relativedelta(years=-1),
                                      end_date=date.today() + relativedelta(days=-10))
        membershipfee.save()
        # La cotisation s'étant terminée il y a 10 jours, il ne devrait pas
        # être à jour de cotistion
        self.assertEqual(member.is_paid_up(), False)

        # Créé une cotisation actuelle
        membershipfee = MembershipFee(member=member, amount=20,
                                      start_date=date.today() +
                                      relativedelta(days=-10),
                                      end_date=date.today() + relativedelta(days=+10))
        membershipfee.save()
        # La cotisation se terminant dans 10 jour, il devrait être à jour
        # de cotisation
        self.assertEqual(member.is_paid_up(), True)

    @freeze_time('2016-01-01')
    def test_adding_running_fee_set_membership_status(self):
        member = Member.objects.create(
            first_name='a', last_name='b', username='c',
            status=Member.MEMBER_STATUS_PENDING)

        # Créé une cotisation passée
        MembershipFee.objects.create(
            member=member, amount=20,
            start_date=date(2015, 12, 12),
            end_date=date(2016, 12, 12))

        member = Member.objects.get(pk=member.pk)
        self.assertEqual(member.status, member.MEMBER_STATUS_MEMBER)

    def test_member_cant_be_created_without_names(self):
        """
        Test qu'un membre ne peut pas être créé sans "noms"
        (prenom, nom) ou pseudo ou nom d'organization
        """
        member = Member(username='blop')
        with self.assertRaises(Exception):
            member.full_clean()
            member.save()

        with self.assertRaises(Exception):
            member.full_clean()
            member.save()



class MemberAdminTests(TestCase):

    def setUp(self):
        #~ Client web
        self.client = Client()
        #~ Créé un superuser
        self.admin_user_password = '1234'
        self.admin_user = Member.objects.create_superuser(
            username='test_admin_user',
            first_name='test',
            last_name='Admin user',
            email='i@mail.com',
            password=self.admin_user_password)
        #~ Connection
        self.assertEqual(self.client.login(
            username=self.admin_user.username, password=self.admin_user_password), True)

    def tearDown(self):
        # Supprime le superuser
        self.admin_user.delete()

    def test_cant_change_username_when_editing(self):
        """
        Vérifie que dans l'admin Django, le champ username n'est pad modifiable
        sur une fiche existante
        """
        #~ Créé un membre
        first_name = 'Gérard'
        last_name = 'Majax'
        username = MemberTestsUtils.get_random_username()
        member = Member(first_name=first_name,
                        last_name=last_name, username=username)
        member.save()

        edit_page = self.client.get('/admin/members/member/%i/' % member.id)
        self.assertNotContains(edit_page,
                               '''<input id="id_username" />''',
                               html=True)

        member.delete()


class MemberTestCallForMembershipCommand(TestCase):

    def setUp(self):
        # Créé un membre
        self.username = MemberTestsUtils.get_random_username()
        self.member = Member(first_name='Richard', last_name='Stallman',
                             username=self.username)
        self.member.save()


    def tearDown(self):
        # Supprime le membre
        self.member.delete()
        MembershipFee.objects.all().delete()

    def create_membership_fee(self, end_date):
        # Créé une cotisation passée se terminant dans un mois
        membershipfee = MembershipFee(member=self.member, amount=20,
                                      start_date=end_date + relativedelta(years=-1),
                                      end_date=end_date)
        membershipfee.save()

    def create_membership_fee(self, end_date):
        # Créé une cotisation se terminant à la date indiquée
        membershipfee = MembershipFee(member=self.member, amount=20,
                                      start_date=end_date + relativedelta(years=-1),
                                      end_date=end_date)
        membershipfee.save()
        return membershipfee

    def do_test_email_sent(self, expected_emails = 1, reset_date_last_call = True):
        # Vide la outbox
        mail.outbox = []
        # Call command
        management.call_command('call_for_membership_fees', stdout=StringIO())
        # Test
        self.assertEqual(len(mail.outbox), expected_emails)
        # Comme on utilise le même membre, on reset la date de dernier envoi
        if reset_date_last_call:
            self.member.date_last_call_for_membership_fees_email = None
            self.member.save()

    def do_test_for_a_end_date(self, end_date, expected_emails=1, reset_date_last_call = True):
        # Supprimer toutes les cotisations (au cas ou)
        MembershipFee.objects.all().delete()
        # Créé la cotisation
        membershipfee = self.create_membership_fee(end_date)
        self.do_test_email_sent(expected_emails, reset_date_last_call)
        membershipfee.delete()

    def test_call_email_sent_at_expected_dates(self):
        # 1 mois avant la fin, à la fin et chaque mois après la fin pendant 3 mois
        self.do_test_for_a_end_date(date.today() + relativedelta(months=+1))
        self.do_test_for_a_end_date(date.today())
        self.do_test_for_a_end_date(date.today() + relativedelta(months=-1))
        self.do_test_for_a_end_date(date.today() + relativedelta(months=-2))
        self.do_test_for_a_end_date(date.today() + relativedelta(months=-3))

    def test_call_email_not_sent_if_active_membership_fee(self):
        # Créé une cotisation se terminant dans un mois
        membershipfee = self.create_membership_fee(date.today() + relativedelta(months=+1))
        # Un mail devrait être envoyé (ne pas vider date_last_call_for_membership_fees_email)
        self.do_test_email_sent(1, False)
        # Créé une cotisation enchainant et se terminant dans un an
        membershipfee = self.create_membership_fee(date.today() + relativedelta(months=+1, years=+1))
        # Pas de mail envoyé
        self.do_test_email_sent(0)

    def test_date_last_call_for_membership_fees_email(self):
        # Créé une cotisation se terminant dans un mois
        membershipfee = self.create_membership_fee(date.today() + relativedelta(months=+1))
        # Un mail envoyé (ne pas vider date_last_call_for_membership_fees_email)
        self.do_test_email_sent(1, False)
        # Tente un deuxième envoi, qui devrait être à 0
        self.do_test_email_sent(0)


class MemberManagerTest(TestCase):
    def setUp(self):
        self.ab = Member.objects.create(
            first_name='a', last_name='b', username='ab', email='ab@ex.com')
        self.cd = Member.objects.create(
            first_name='c', last_name='d', username='cd', email='cd@ex.com')
        self.ef = Member.objects.create(
            first_name='e', last_name='f', username='ef', email='ef@ex.com')
        self.gh = Member.objects.create(
            first_name='g', last_name='h', username='gh', email='gh@ex.com')

        MembershipFee.objects.create(member=self.ab, amount=20,
                      start_date=date(2015, 11, 11),
                      end_date=date(2016, 11, 11))

        MembershipFee.objects.create(member=self.cd, amount=20,
                      start_date=date(2016, 1, 1),
                      end_date=date(2016, 1, 1))

        OfferSubscription.objects.create(
            subscription_date=date(2016, 2, 2),
            member=self.ef,
            offer=Offer.objects.create(
                name='fu',
                period_fees=0,
                initial_fees=0
            ),
        )

    @freeze_time('2016-10-01')
    def test_could_be_deleted(self):
        deletion_set = set(Member.objects.could_be_deleted())

        # late on fee (-> delete)
        self.assertIn(self.cd, deletion_set)

        # no fee at all (-> delete)
        self.assertIn(self.gh, deletion_set)

        # running fee (-> no delete)
        self.assertNotIn(self.ab, deletion_set)

        # running service (even if no fee) (-> no delete)
        self.assertNotIn(self.ef, deletion_set)


class MemberTestsUtils(object):

    @staticmethod
    def get_random_username():
        """
        Renvoi une clé aléatoire pour un utilisateur LDAP
        """
        return 'coin_test_' + os.urandom(8).encode('hex')


class TestValidators(TestCase):
    def test_valid_chatroom(self):
        chatroom_url_validator('irc://irc.example.com/#chan')
        with self.assertRaises(ValidationError):
            chatroom_url_validator('http://#faimaison@irc.geeknode.org')


class MembershipFeeTests(TestCase):
    def test_mandatory_start_date(self):
        member = Member(first_name='foo', last_name='foo', password='foo', email='foo')
        member.save()

        # If there is no start_date clean_fields() should raise an
        # error but not clean().
        membershipfee = MembershipFee(member=member, amount=15)
        self.assertRaises(ValidationError, membershipfee.clean_fields)
        self.assertIsNone(membershipfee.clean())

        # If there is a start_date, everything is fine.
        membershipfee = MembershipFee(member=member, amount=15,
                start_date=date.today())
        self.assertIsNone(membershipfee.clean_fields())
        self.assertIsNone(membershipfee.clean())

        member.delete()
