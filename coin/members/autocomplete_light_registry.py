# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import autocomplete_light
from models import Member

# This will generate a MemberAutocomplete class
autocomplete_light.register(Member,
                            # Just like in ModelAdmin.search_fields
                            search_fields=[
                                '^first_name', '^last_name', 'organization_name',
                                '^username', '^nickname'],
                            attrs={
                                # This will set the input placeholder attribute:
                                'placeholder': 'Nom/Prénom/Pseudo (min 3 caractères)',
                                # Nombre minimum de caractères à saisir avant de compléter.
                                # Fixé à 3 pour ne pas qu'on puisse avoir accès à la liste de tous les membres facilement quand on n'est pas superuser.
                                'data-autocomplete-minimum-characters': 3,
                            },
)
