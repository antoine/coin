# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0009_auto_20141008_2244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='status',
            field=models.CharField(default='member', max_length=50, verbose_name='statut', choices=[('member', 'Adh\xe9rent'), ('not_member', 'Non adh\xe9rent'), ('pending', "Demande d'adh\xe9sion")]),
        ),
    ]
