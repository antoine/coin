from registration.backends.hmac.views import ActivationView, RegistrationView


class MemberActivationView(ActivationView):
    template_name = 'members/registration/activate.html'
    def get_success_url(self, user):
        return ('members:registration_activation_complete', (), {})

class MemberRegistrationView(RegistrationView):
    success_url = ('member:registration_activation_complete', (), {})
    email_subject_template = 'members/registration/activation_email_subject.txt'
    email_body_template = 'members/registration/activation_email.txt'
    template_name = 'members/registration/registration_form.html'

    def register(self, form):
        new_user = super(MemberRegistrationView, self).register(form)
        new_user.status = new_user.MEMBER_STATUS_PENDING
        return new_user.save()

    def get_success_url(self, user):
        return ('members:registration_complete', (), {})
