# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.apps import AppConfig


class ISPdatabaseConfig(AppConfig):
    name = 'coin.isp_database'
    verbose_name = 'Identité du FAI'
