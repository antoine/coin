# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0011_auto_20170309_1247'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chatroom',
            name='url',
            field=models.CharField(max_length=256, verbose_name='URL', validators=[django.core.validators.RegexValidator(regex=re.compile('(?P<protocol>\\w+://)(?P<server>[\\w\\.]+)/(?P<channel>.*)'), message='Enter a value of the form  <proto>://<server>/<channel>')]),
        ),
    ]
