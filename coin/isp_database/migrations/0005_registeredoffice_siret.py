# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import localflavor.fr.models


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0004_ispinfo_support_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='registeredoffice',
            name='siret',
            field=localflavor.fr.models.FRSIRETField(default='', max_length=14),
            preserve_default=False,
        ),
    ]
