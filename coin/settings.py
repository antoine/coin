# -*- coding: utf-8 -*-

from settings_base import *

# Surcharge les paramètres en utilisant le fichier settings_local.py
try:
    from settings_local import *
except ImportError:
    pass

TEMPLATE_DIRS = EXTRA_TEMPLATE_DIRS + TEMPLATE_DIRS
INSTALLED_APPS = INSTALLED_APPS + EXTRA_INSTALLED_APPS
