# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0003_configuration_comment'),
    ]

    operations = [
        migrations.CreateModel(
            name='SimpleDSL',
            fields=[
                ('configuration_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='configuration.Configuration')),
                ('phone_number', models.CharField(help_text='Phone number associated to the DSL line', max_length=20, verbose_name='phone number')),
            ],
            options={
                'verbose_name': 'DSL line',
            },
            bases=('configuration.configuration',),
        ),
    ]
