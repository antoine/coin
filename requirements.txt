Django>=1.8.17,<1.9
psycopg2==2.5.4
# To match django-ldapdb version:
python-ldap>=3.0,<3.1
wsgiref==0.1.2
pydotplus==2.0.2
python-dateutil==2.2
django-autocomplete-light>=2.2.10,<2.3
django-activelink==0.4
html2text
django-polymorphic==0.7.2
django-sendfile==0.3.10
django-hijack>=2.1.10,<2.2
django-localflavor==1.1
django-netfields>=0.4,<0.5
# Latest with Django <= 1.8 support:
django-ldapdb>=0.8.0,<0.9
django-multiselectfield>=0.1.5
feedparser
six==1.10.0
WeasyPrint==0.31
freezegun==0.3.8
django-registration==2.2
pytz>=2018.5
unidecode>=1.0,<1.1
django-debug-toolbar>=1.9.1,<1.10
# Higher requires Django>=1.9
django-extensions>=2.0,<2.1
