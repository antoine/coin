# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404
from django.views.generic.edit import UpdateView
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.decorators import method_decorator

from .models import DSLConfiguration


class DSLView(SuccessMessageMixin, UpdateView):
    model = DSLConfiguration
    fields = ['password']
    success_message = "Mot de passe mis à jour avec succès !"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DSLView, self).dispatch(*args, **kwargs)

    def get_object(self):
        if self.request.user.is_superuser:
            return get_object_or_404(DSLConfiguration, pk=self.kwargs.get("pk"))
        # For normal users, ensure the VPN belongs to them.
        return get_object_or_404(DSLConfiguration, pk=self.kwargs.get("pk"),
                                 offersubscription__member=self.request.user)
