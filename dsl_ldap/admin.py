# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin

from .models import RadiusGroup, DSLConfiguration
from coin.configuration.admin import ConfigurationAdminFormMixin
from coin.utils import delete_selected


class DSLConfigurationInline(admin.StackedInline):
    model = DSLConfiguration
    fields = ('offersubscription', 'phone_number', 'activated', 'radius_group', 'login', 'password')
    readonly_fields = ['configuration_ptr', 'login']


class DSLConfigurationAdmin(ConfigurationAdminFormMixin, PolymorphicChildModelAdmin):
    base_model = DSLConfiguration
    list_display = ('offersubscription', 'activated', 'full_login',
                    'radius_group', 'comment')
    list_filter = ('activated', 'radius_group')
    search_fields = ('login',
                     # TODO: searching on member directly doesn't work
                     'offersubscription__member__first_name',
                     'offersubscription__member__last_name',
                     'offersubscription__member__email')
    actions = (delete_selected, "activate", "deactivate")
    fields = ('offersubscription', 'phone_number', 'comment',
              'activated', 'radius_group', 'login', 'password')
    inline = DSLConfigurationInline

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.login != "":
            return ['login',]
        else:
            return []

    def set_activation(self, request, queryset, value):
        count = 0
        # We must update each object individually, because we want to run
        # the save() method to update the backend.
        for vpn in queryset:
            if vpn.activated != value:
                vpn.activated = value
                vpn.full_clean()
                vpn.save()
                count += 1
        action = "activated" if value else "deactivated"
        msg = "{} DSL line(s) {}.".format(count, action)
        self.message_user(request, msg)

    def activate(self, request, queryset):
        self.set_activation(request, queryset, True)
    activate.short_description = "Activate selected DSL lines"

    def deactivate(self, request, queryset):
        self.set_activation(request, queryset, False)
    deactivate.short_description = "Deactivate selected DSL lines"

admin.site.register(RadiusGroup,)
admin.site.register(DSLConfiguration, DSLConfigurationAdmin)
