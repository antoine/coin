# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import netfields.fields
import coin.validation


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0004_auto_20161015_1837'),
    ]

    operations = [
        migrations.CreateModel(
            name='HousingConfiguration',
            fields=[
                ('configuration_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='configuration.Configuration')),
                ('activated', models.BooleanField(default=False, verbose_name='activ\xe9')),
                ('ipv4_endpoint', netfields.fields.InetAddressField(validators=[coin.validation.validate_v4], max_length=39, blank=True, help_text='Adresse IPv4 utilis\xe9e par d\xe9faut sur le Housing', null=True, verbose_name='IPv4')),
                ('ipv6_endpoint', netfields.fields.InetAddressField(validators=[coin.validation.validate_v6], max_length=39, blank=True, help_text='Adresse IPv6 utilis\xe9e par d\xe9faut sur le Housing', null=True, verbose_name='IPv6')),
                ('vlan', models.IntegerField(null=True, verbose_name='vlan id')),
            ],
            options={
                'verbose_name': 'Housing',
            },
            bases=('configuration.configuration',),
        ),
    ]
