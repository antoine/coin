# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vps', '0002_auto_20170803_0350'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vpsconfiguration',
            name='console',
        ),
        migrations.AddField(
            model_name='console',
            name='vps',
            field=models.OneToOneField(default=1, verbose_name='vps', to='vps.VPSConfiguration'),
            preserve_default=False,
        ),
    ]
